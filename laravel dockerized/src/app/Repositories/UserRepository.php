<?php
namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * class UserRepository
 */
class UserRepository implements RepositoryInterface
{
    public function findAll():Collection
    {
        return User::all();
    }

    public function findOneById(int $id): User
    {
        return User::find($id);
    }

    public function findByCriteria(array $criteria): Collection
    {
        return User::all();
    }
}
