<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * interface RepositoryInterface
 */
interface RepositoryInterface {
    /**
     * Undocumented function
     *
     * @return Collection
     */
    function findAll():Collection;

    /**
     * Undocumented function
     *
     * @param int $id
     * @return Model
     */
    function findOneById(int $id):Model;

    /**
     * Undocumented function
     *
     * @param array $criteria
     * @return Collection
     */
    function findByCriteria(array $criteria):Collection;
}
