<?php

namespace App\View\Components\admin;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class AppAdmin extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View
    {
        return view('components.admin.app-admin');
    }
}
