<?php

namespace App\View\Components\admin\users;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class EditUser extends Component
{
    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.admin.users.edit-user');
    }
}
