<?php

namespace App\View\Components\admin\channel;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Channel extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.admin.channel.channel');
    }
}
