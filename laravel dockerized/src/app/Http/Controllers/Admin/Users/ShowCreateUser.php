<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShowCreateUser extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        return view('components.admin.users.create-user');
    }
}
