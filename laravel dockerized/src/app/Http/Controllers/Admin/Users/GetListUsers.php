<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\Users\GetListUsersRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class GetListUsers extends Controller
{
    public function __construct(private UserRepository $userRepository)
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        return View::make('components.admin.users.users', ['users' => DB::table('users')->paginate(6)]);
    }
}
