<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ShowEditUser extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Handle the incoming request.
     */
    public function __invoke(User $user)
    {
        return view('components.admin.users.edit-user', ['user' => $user]);
    }
}
