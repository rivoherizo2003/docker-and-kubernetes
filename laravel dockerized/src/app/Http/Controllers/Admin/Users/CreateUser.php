<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UserCreateRequest;
use App\Models\User;

class CreateUser extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * @param CreateUserRequest $createUserRequest
     * @return void
     */
    public function __invoke(UserCreateRequest $createUserRequest)
    {
        $validated = $createUserRequest->validated();
        User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => $validated['password']
        ]);

        return redirect()->route('list-users');
    }
}
