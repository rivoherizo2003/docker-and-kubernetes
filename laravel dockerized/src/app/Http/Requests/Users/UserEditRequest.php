<?php

namespace App\Http\Requests\Users;

class UserEditRequest extends UserRequest{
    public function messages()
    {
        return [
            'name.min' => ':input is too short, minimum 10 letters',
            'email.unique' => ':input already exists',
            'email.required' => 'You must give your email',
            'email.email' => 'This email :input is incorrect'
        ];
    }
}
