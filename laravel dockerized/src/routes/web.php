<?php

use App\Http\Controllers\Admin\Users\CreateUser;
use App\Http\Controllers\Admin\Users\DeleteUser;
use App\Http\Controllers\Admin\Users\GetListUsers;
use App\Http\Controllers\Admin\Users\SaveUser;
use App\Http\Controllers\Admin\Users\ShowCreateUser;
use App\Http\Controllers\Admin\Users\ShowEditUser;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ShowChannel;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('front.home');
})->name('home');

///////// ADMIN ////////////
Route::prefix('admin')->group(function () {
    /////// USERS /////////////
    Route::post('/create-user', CreateUser::class)->name('create-user');

    Route::get('/show-create-user', ShowCreateUser::class)->name('show-create-user');

    Route::get('/show-edit-user/{user}', ShowEditUser::class)->name('show-edit-user');

    Route::get('/list-users', GetListUsers::class)->name('list-users');

    Route::post('/save-user', SaveUser::class)->name('save-user');

    Route::get('/delete-user/{user}', DeleteUser::class)->name('delete-user');
    ///////////////////////////

    ///////// CHANNEL //////////////
    Route::get('/channel', ShowChannel::class)->name('show-channel');
    ///////////////////////////////

    /////////////// MAILBOX ////////
    Route::get('/mailbox', function () {
        return view('components.admin.mails.mail-box');
    })->middleware(['auth', 'verified'])->name('mailbox');
    ////////////////////////////////
});
/////////////////////////////




Route::get('/logout', [AuthenticatedSessionController::class, 'destroy'])->middleware(['auth', 'verified'])->name('logout');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
