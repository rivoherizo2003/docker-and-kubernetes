<x-admin.app-admin>
    <x-slot:users>active</x-slot:users>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Create user</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a class="btn btn-app bg-secondary" href="{{ route('list-users') }}">
                                <i class="fas fa-angle-double-left"></i> Back
                            </a>
                        </li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">New user</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('create-user') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Name:</label>
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                                    </div>
                                    <input type="text" autofocus required
                                        class="form-control @error('name') is-invalid @enderror"
                                        name="name" placeholder="Full name" value="{{ @old('name') }}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Mail:</label>
                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                    </div>
                                    <input type="email" name="email"
                                        class="form-control @error('email') is-invalid @enderror" placeholder="type email"
                                        data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask="" inputmode="text"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label>Password:</label>
                                @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-lock"></i></span>
                                    </div>
                                    <input type="password" value="123456789"
                                        class="form-control @error('password') is-invalid @enderror"
                                        placeholder="Password" name="password" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Confirm password:</label>
                                @error('password_confirmation')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-lock"></i></span>
                                    </div>
                                    <input type="password" value="123456789"
                                        class="form-control @error('password') is-invalid @enderror"
                                        placeholder="Retype password" name="password_confirmation" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <a href="{{ route('show-create-user') }}"
                                            class="btn btn-block btn-warning btn-flat"><i class="fas fa-eject"></i>
                                            Cancel</a>
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <button type="submit" class="btn btn-block btn-success btn-block btn-flat"><i
                                                class="fas fa-folder-plus"></i> Create</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-admin.app-admin>
