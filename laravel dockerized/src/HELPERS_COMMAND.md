## Create controller
```bash
docker compose run artisan make:controller CreateUser --invokable
```

## Build assets
```bash
docker compose run --rm npm -i run build
```

## Create a component in sub folder
```bash
docker compose run artisan make:component front/Home
```

## Run migration
```bash
docker compose run artisan migrate
```

## Create seeder for the User model
```bash
docker compose run artisan make:seeder UserSeeder
```

## Launch seeder for the User model
```bash
docker compose run artisan db:seed --class==UserSeeder
```
