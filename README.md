## Serve only needed container
```bash
docker-compose up lrv_server lrv_php lrv_mysql
``` 

## Install laravel from container composer
```bash
docker-compose run --rm lrv_composer create-project --prefer-dist laravel/laravel .
```

<h3>KUBERNETES</h3>
========================

## Start minikube before all commands (locally)
```bash
minikube start && minikube dashboard
```

# Imperative deployment
## K8s Create new object deployment 
```bash
kubectl create deployment first-app --image=ozirehdocker/kb-first-app
```
```bash
kubectl get deployments
```
```bash
kubectl get pods
```

## Expose pods by services
```bash
kubectl expose deployment first-app --type=LoadBalancer --port=8080
```

## Command to give us access to the service so we can reach from our local machine. This command is not used in cloud service
```bash
minikube service first-app
```

<hr>

## Scaling in action
```bash
kubectl scale deployment/first-app --replicas=3
```

## Update image for a deployment
```bash
kubectl set image deployment/first-app container-name=repo-image:tag
```

## View update after image updates
```bash
kubectl rollout status deployment/first-app
```

## Rollback deployment
```bash
kubectl rollout undo deployment/first-app
```

## Rollback history
```bash
kubectl rollout history deployment/first-app
```

## Rollback to old version
```bash
kubectl rollout undo deployment/first-app --to-revision=1
```

## delete service
```bash
kubectl delete service first-app
```

## delete deployment
```bash
kubectl delete service deployment first-app
```

# Declarative deployment

## Launch deployment with a config file
```bash
kubectl apply -f=deployment.yaml
```

## Create service in order to expose our pod to outside
```bash
kubectl apply -f service.yaml
```
## Get services
```bash
kubectl get services
```
## Expose service so our pod will be exposed to outside
```bash
minikube service backend
```

## Delete deployments
```bash
kubectl delete -f=deployment.yml -f=service.yml
```
## Make kubectl to config our eks cluster
Rename your .kube/config file in order to save it because we will use the default to reach the cluster in aws

## Launch commands to reach eks by kubectl
1- Install and configure aws cli

2- Send command to you active cluster
```bash
aws eks us-east-2 update-kubeconfig --name clusterName
```

This command will update the file .kube/config in order to send command with kubectl to the cluster


